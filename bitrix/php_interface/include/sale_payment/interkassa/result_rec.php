<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * Разработка модуля - Divasoft, inc.
 * http://divasoft.ru
 * Версия 1.0
 * 2016
 * 
 * Библиотека "Interkassa API for PHP"
 * https://github.com/kpobococ/ik-php
 */

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
use Bitrix\Main\IO\Path;
include(GetLangFileName(dirname(__FILE__) . "/", "/lang.php"));

include __DIR__.'/lib/interkassa.php';
Interkassa::register();

$shop = Interkassa_Shop::factory(array(
    'id' => CSalePaySystemAction::GetParamValue("MERCHANT_ID"),
    'secret_key' => ((CSalePaySystemAction::GetParamValue("PAYMENT_VALUE")=="test_interkassa_test_xts")?CSalePaySystemAction::GetParamValue("SECRET_TEST_KEY"):CSalePaySystemAction::GetParamValue("SECRET_KEY"))
));


if (count($_REQUEST)) {
    try {
        $status = $shop->receiveStatus($_REQUEST); // POST is used by default
    } catch (Interkassa_Exception $e) {
        // The signature was incorrect, send a 400 error to interkassa
        // They should resend payment status request until they receive a 200 status
        header('HTTP/1.0 400 Bad Request');
        print_r($e);
        exit;
    }

    $payment = $status->getPayment();
    $orderId = $payment->getId();
    $merchant_price = $payment->getAmount();
    //$time = date("d.m.Y H:i:s", $status->getTimestamp());
    
    if (!($arOrder = CSaleOrder::GetByID(intval($orderId)))) {
        header('HTTP/1.0 400 Bad Request');
        exit;
    }

    CSalePaySystemAction::InitParamArrays($arOrder, $arOrder["ID"]);
    
    if ($arOrder["PAYED"] == "N") {
        $arFields = array(
            "PS_STATUS" => "Y",
            "PS_STATUS_CODE" => "-",
            "PS_STATUS_DESCRIPTION" => $orderId,
            "PS_STATUS_MESSAGE" => $status->getState(),
            "PS_SUM" => $merchant_price,
            "PS_CURRENCY" => "RUB",
            "PS_RESPONSE_DATE" => date(CDatabase::DateFormatToPHP(CLang::GetDateFormat("FULL", LANG))),
            "USER_ID" => $arOrder["USER_ID"],
        );

        if (CSaleOrder::Update($arOrder["ID"], $arFields)) {
            if ($arOrder["PRICE"] == $merchant_price) {
                CSaleOrder::PayOrder($arOrder["ID"], "Y", false);
                CSaleOrder::StatusOrder($arOrder["ID"], "F");
                exit("OK");
            } else {
                header('HTTP/1.0 400 Bad Request');
                exit();
            }
        }
    } else {
        header('HTTP/1.0 400 Bad Request');
        exit();
    }
}
header('HTTP/1.0 400 Bad Request');
exit();