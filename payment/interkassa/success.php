<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
/**
 * Страница для отображения удачной покупки
 * 
 * Разработка модуля - Divasoft, inc.
 * http://divasoft.ru
 * Версия 1.0
 * 2016
 */
use \Bitrix\Main\Localization\Loc; Loc::loadMessages(__FILE__);

echo Loc::getMessage("XBILL_OK");
echo '<p><a href="/personal/order/"> ' . Loc::getMessage("PAYEER_USER_ORDERS") . '</a></p>';

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>

