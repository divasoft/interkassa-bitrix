<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
/**
 * Универсальный обработчик оплаты, если платежных систем на основе одного обработчика больше чем одна штука.
 * 
 * Разработка модуля - Divasoft, inc.
 * http://divasoft.ru
 * Версия 1.0
 * 2016
 */
/*
$_REQUEST = Array(
    'ik_co_id'=> '',
    'ik_co_prs_id' => '',
    'ik_inv_id' => ,
    'ik_inv_st' => 'success',
    'ik_inv_crt' => '2016-02-05 17:06:04',
    'ik_inv_prc' => '2016-02-05 17:06:04',
    'ik_trn_id' => '',
    'ik_pm_no' => 218,
    'ik_pw_via' => 'test_interkassa_test_xts',
    'ik_am' => 200,
    'ik_co_rfn' => 200,
    'ik_ps_price' => 206,
    'ik_cur' => 'RUB',
    'ik_desc' => 'Оплата заказа №218',
    'ik_x_baggage' => '',
    'ik_sign' => ''
);
/*
// Кусок кода для отладки входящих параметров
$req_dump = print_r($_REQUEST, TRUE);
$fp = fopen('request.log', 'a');
fwrite($fp, $req_dump);
fclose($fp);*/

$psID = 14; // PAY_SYSTEM_ID, эту переменную запросим из заказа.
$ptID = 1; // PERSON_TYPE_ID, эту переменную можно установить и не менять, если один тип плательщика. В основном это "Физ.лицо". Юр лица оплачивают через Р/С
$ordID = intval($_REQUEST['ik_pm_no']);

use Bitrix\Main\Application;
use Bitrix\Main\Page\Asset;
use Bitrix\Sale\Order;
use Bitrix\Main\Localization\Loc;
Bitrix\Main\Loader::includeModule('sale');

if ($ordID) {
        // Новый способ, но чисто на 1 оплату, если будет несколько оплат, то надо допилить.
        $dbPayment = \Bitrix\Sale\Internals\PaymentTable::getList(array(
                    'select' => array('PAY_SYSTEM_ID'), //'PAY_SYSTEM_NAME', 
                    'filter' => array('ORDER_ID' => $ordID)
        ));
        if ($arPayment = $dbPayment->fetch()) {
            $psID = $arPayment['PAY_SYSTEM_ID'];
            //print_r($arPayment); 
        }
        // PERSON_TYPE_ID нужно еще дополнительно запросить из заказа. если он один, то впишите вручную.

        // Старый способ, если изменится через админку платежная система, то нужно использовать функции ядра D7. Иначе будет возвращен старый ID, и оплата не пройдет.
	/*$arOrder = CSaleOrder::GetByID($ordID);
	$psID = $arOrder['PAY_SYSTEM_ID'];
	$ptID = $arOrder['PERSON_TYPE_ID'];*/
}

$APPLICATION->IncludeComponent(
	'bitrix:sale.order.payment.receive',
	'',
	Array(
		'PAY_SYSTEM_ID' => $psID,
		'PERSON_TYPE_ID' => $ptID
	),
false
);
 require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>