Обработчик платежной системы для 1С-Битрикс

Провайдер <https://interkassa.com/>

Распаковать в корень сайта.  
Все инструкции по настройке будут при выборе платежного обработчика.  
Кодировка проекта - UTF-8

------------------

За основу взята библиотека: <https://github.com/kpobococ/ik-php>  
Официальная документация: <https://www.interkassa.com/files/docs/IK2.SCI.Protocol.v1.0.0.ru.pdf>  
Список платежных систем: https://api.interkassa.com/v1/paysystem-input-payway  