<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

/**
 * Разработка модуля - Divasoft, inc.
 * http://divasoft.ru
 * Версия 1.0
 * 2016
 */
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$psTitle = Loc::getMessage("INTERKASSA_MAIN_TITLE");
$psDescription = str_replace("#RESULT_URL#", "http://" . $_SERVER['SERVER_NAME'] . "/payment/interkassa/st.php", Loc::getMessage("INTERKASSA_MAIN_DESCR"));
$psDescription = str_replace("#OK_URL#", "http://" . $_SERVER['SERVER_NAME'] . "/payment/interkassa/success.php", $psDescription);
$psDescription = str_replace("#ERR_URL#", "http://" . $_SERVER['SERVER_NAME'] . "/payment/interkassa/fail.php", $psDescription);

$psTypeDescr = Loc::getMessage("INTERKASSA_TYPE_DESCR");

// Загружаем все платежные системы
$interkassaPayments = array("_dvsmanual"=>array("NAME"=>"Пользователь сам выбирает систему оплаты"));
$jsonPaymentsString = json_decode(file_get_contents('https://api.interkassa.com/v1/paysystem-input-payway'));
foreach ($jsonPaymentsString->data as $payInfo) {
    $interkassaPayments[$payInfo->als]=array("NAME"=>("[".$payInfo->name[0]->v."] ".$payInfo->ps." - ".$payInfo->curAls." - ".$payInfo->inAls) );
}
ksort($interkassaPayments);
//pr($interkassaPayments);

/* Старый список
$interkassaPayments = array(
    'privatterm_liqpay_merchant_uah' => array('NAME' => 'Терминалы Приватбанка - LiqPay - Мерчант'),
    'anelik_w1_merchant_rub' => array('NAME' => 'Anelik - Единый кошелек - Мерчант'),
    'beeline_w1_merchant_rub' => array('NAME' => 'Билайн - Единый кошелек - Мерчант'),
    'contact_w1_merchant_rub' => array('NAME' => 'CONTACT - Единый кошелек - Мерчант'),
    'lider_w1_merchant_rub' => array('NAME' => 'ЛИДЕР - Единый кошелек - Мерчант'),
    'megafon_w1_merchant_rub' => array('NAME' => 'Мегафон - Единый кошелек - Мерчант'),
    'mobileretails_w1_merchant_rub' => array('NAME' => 'Салоны связи - Единый кошелек - Мерчант'),
    'mts_w1_merchant_rub' => array('NAME' => 'МТС - Единый кошелек - Мерчант'),
    'qiwiwallet_w1_merchant_rub' => array('NAME' => 'Qiwi Кошелек - Единый кошелек - Мерчант'),
    'ruspost_w1_merchant_rub' => array('NAME' => 'Почта Росси - Единый кошелек - Мерчант'),
    'rusterminal_w1_merchant_rub' => array('NAME' => 'Терминалы Росси - Единый кошелек - Мерчант'),
    'unistream_w1_merchant_rub' => array('NAME' => 'Юнистрим - Единый кошелек - Мерчант'),
    'webmoney_merchant_wmb' => array('NAME' => 'WebMoney WMB - Мерчант'),
    'webmoney_merchant_wme' => array('NAME' => 'WebMoney WME - Мерчант'),
    'webmoney_merchant_wmg' => array('NAME' => 'WebMoney WMG - Мерчант'),
    'webmoney_merchant_wmr' => array('NAME' => 'WebMoney WMR - Мерчант'),
    'webmoney_merchant_wmu' => array('NAME' => 'WebMoney WMU - Мерчант'),
    'webmoney_merchant_wmz' => array('NAME' => 'WebMoney WMZ - Мерчант'),
    'nsmep_smartpay_invoice_uah' => array('NAME' => 'НСМЕП - SmartPay - Выставление счета'),
    'yandexmoney_merchant_rub' => array('NAME' => 'Yandex.Money - Мерчант'),
    'zpayment_merchant_rub' => array('NAME' => 'Z-payment - Мерчант'),
    'sbrf_rusbank_receipt_rub' => array('NAME' => 'Сбербанк РФ - Российский банк - Квитанция'),
    'webmoney_invoice_wmz' => array('NAME' => 'WebMoney WMZ - Выставление счета'),
    'webmoney_invoice_wmu' => array('NAME' => 'WebMoney WMU - Выставление счета'),
    'webmoney_invoice_wmr' => array('NAME' => 'WebMoney WMR - Выставление счета'),
    'webmoney_invoice_wmg' => array('NAME' => 'WebMoney WMG - Выставление счета'),
    'webmoney_invoice_wme' => array('NAME' => 'WebMoney WME - Выставление счета'),
    'webmoney_invoice_wmb' => array('NAME' => 'WebMoney WMB - Выставление счета'),
    'webcreds_merchant_rub' => array('NAME' => 'WebCreds - Мерчант'),
    'w1_merchant_usdw1_w1_merchant_usd' => array('NAME' => 'Единый кошелек USD - Мерчант'),
    'w1_merchant_uahw1_w1_merchant_uah' => array('NAME' => 'Единый кошелек UAH - Мерчант'),
    'w1_merchant_rubw1_w1_merchant_rub' => array('NAME' => 'Единый кошелек RUB - Мерчант'),
    'w1_merchant_eurw1_w1_merchant_eur' => array('NAME' => 'Единый кошелек EUR - Мерчант'),
    'visa_liqpay_merchant_usd' => array('NAME' => 'Visa UDS - LiqPay - Мерчант'),
    'visa_liqpay_merchant_rub' => array('NAME' => 'Visa RUB - LiqPay - Мерчант'),
    'visa_liqpay_merchant_eur' => array('NAME' => 'Visa EUR - LiqPay - Мерчант'),
    'ukrbank_receipt_uah' => array('NAME' => 'Украинский банк - Квитанция'),
    'ukash_w1_merchant_usd' => array('NAME' => 'Ukash - Единый кошелек - Мерчант'),
    'rusbank_receipt_rub' => array('NAME' => 'Российский банк - Квитанция'),
    'rbkmoney_merchant_rub' => array('NAME' => 'RBK Money - Мерчант'),
    'privat24_merchant_usd' => array('NAME' => 'Privat24 USD - Мерчант'),
    'privat24_merchant_uah' => array('NAME' => 'Privat24 UAH - Мерчант'),
    'privat24_merchant_eur' => array('NAME' => 'Privat24 EUR - Мерчант'),
    'perfectmoney_merchant_usd' => array('NAME' => 'PerfectMoney USD - Мерчант'),
    'perfectmoney_merchant_eur' => array('NAME' => 'PerfectMoney EUR - Мерчант'),
    'moneymail_merchant_usd' => array('NAME' => 'MoneyMail USD - Мерчант'),
    'moneymail_merchant_rub' => array('NAME' => 'MoneyMail RUB - Мерчант'),
    'moneymail_merchant_eur' => array('NAME' => 'MoneyMail EUR - Мерчант'),
    'monexy_merchant_usd' => array('NAME' => 'MoneXy USD - Мерчант'),
    'monexy_merchant_uah' => array('NAME' => 'MoneXy UAH - Мерчант'),
    'monexy_merchant_rub' => array('NAME' => 'MoneXy RUB - Мерчант'),
    'monexy_merchant_eur' => array('NAME' => 'MoneXy EUR - Мерчант'),
    'mastercard_liqpay_merchant_usd' => array('NAME' => 'Mastercard USD - LiqPay - Мерчант'),
    'mastercard_liqpay_merchant_rub' => array('NAME' => 'Mastercard RUB - LiqPay - Мерчант'),
    'mastercard_liqpay_merchant_eur' => array('NAME' => 'Mastercard EUR - LiqPay - Мерчант'),
    'liqpay_merchant_usd' => array('NAME' => 'LiqPay USD - Мерчант'),
    'liqpay_merchant_uah' => array('NAME' => 'LiqPay UAH - Мерчант'),
    'liqpay_merchant_rub' => array('NAME' => 'LiqPay RUB - Мерчант'),
    'liqpay_merchant_eur' => array('NAME' => 'LiqPay EUR - Мерчант'),
    'libertyreserve_merchant_usd' => array('NAME' => 'Liberty Reserve USD - Мерчант'),
    'libertyreserve_merchant_eur' => array('NAME' => 'Liberty Reserve EUR - Мерчант'),
    'eurobank_receipt_usd' => array('NAME' => 'Wire Transfer USD - Квитанция'),
    'paypal_merchant_usd' => array('NAME' => 'Paypal USD - Мерчант'),
    'alfaclick_w1_merchant_rub' => array('NAME' => 'Альфаклик (Альфабанк) - Единый кошелек - Мерчант'),
    'interkassa_voucher_usd' => array('NAME' => 'Интеркасса USD - Ваучер'),
    'visa_liqpay_merchant_uah' => array('NAME' => 'Visa - LiqPay - Мерчант'),
    'mastercard_liqpay_merchant_uah' => array('NAME' => 'Mastercard - LiqPay - Мерчант'),
    'rbkmoney_merchantx_rub' => array('NAME' => 'RBK Money - Мерчант'),
    'telemoney_merchant_rub' => array('NAME' => 'Telemoney - Мерчант'),
    'ukrterminal_webmoneyuga_terminal_uah' => array('NAME' => 'Терминалы Украины - Webmoney UGA - Терминал'),
    'test_interkassa_test_xts' => array('NAME' => 'Тестовая платежная система - Интеркасса - Тест')
);
*/

$arPSCorrespondence = array(
    "MERCHANT_ID" => array(
        "NAME" => Loc::getMessage("MERCHANT_ID"),
        "DESCR" => Loc::getMessage("MERCHANT_ID_DESCR"),
        "VALUE" => "",
        "TYPE" => ""
    ),
    "SECRET_KEY" => array(
        "NAME" => Loc::getMessage("SECRET_KEY"),
        "DESCR" => Loc::getMessage("SECRET_KEY_DESCR"),
        "VALUE" => "",
        "TYPE" => ""
    ),
    "SECRET_TEST_KEY" => array(
        "NAME" => Loc::getMessage("SECRET_TEST_KEY"),
        "DESCR" => Loc::getMessage("SECRET_TEST_KEY_DESCR"),
        "VALUE" => "",
        "TYPE" => ""
    ),
    "PAYMENT_VALUE" => array(
        "NAME" => GetMessage("SALE_TYPE_PAYMENT"),
        "DESCR" => GetMessage("SALE_TYPE_PAYMENT_DESCR"),
        "TYPE" => "SELECT",
        "VALUE" => $interkassaPayments,
    ),
    "ORDER_EMAIL" => array(
      "NAME" => Loc::getMessage("ORDER_EMAIL"),
      "DESCR" => Loc::getMessage("ORDER_EMAIL_DESCR"),
      "VALUE" => "",
      "TYPE" => "ORDER"
    ),
    "ORDER_ID" => array(
        "NAME" => Loc::getMessage("ORDER_ID"),
        "DESCR" => "",
        "VALUE" => "ID",
        "TYPE" => "ORDER"
    ),
    "SHOULD_PAY" => array(
        "NAME" => Loc::getMessage("SHOULD_PAY"),
        "DESCR" => "",
        "VALUE" => "SHOULD_PAY",
        "TYPE" => "ORDER"
    ),
    "CURRENCY" => array(
        "NAME" => Loc::getMessage("CURRENCY"),
        "DESCR" => Loc::getMessage("CURRENCY_DESCR"),
        "VALUE" => "CURRENCY",
        "TYPE" => "ORDER"
    ),
    "ORDER_DESCRIPTION" => array(
        "NAME" => Loc::getMessage("ORDER_DESCRIPTION"),
        "DESCR" => Loc::getMessage("ORDER_DESCRIPTION_DESCR"),
        "VALUE" => Loc::getMessage("ORDER_DESCRIPTION_VALUE"),
        "TYPE" => ""
    ),
    /*"INTERKASSA_TEST" => array(
        "NAME" => Loc::getMessage("INTERKASSA_TEST"),
        "DESCR" => Loc::getMessage("INTERKASSA_TEST_DESCR"),
        "VALUE" => array(
            'Y' => array('NAME' => Loc::getMessage("INTERKASSA_YES")),
            'N' => array('NAME' => Loc::getMessage("INTERKASSA_NO"))
        ),
        "TYPE" => "SELECT"
    ),*/
    "AMOUNT_TYPE" => array(
        "NAME" => Loc::getMessage("AMOUNT_TYPE"),
        "DESCR" => Loc::getMessage("AMOUNT_TYPE_DESCR"),
        "VALUE" => array(
            'invoice' => array('NAME' => 'invoice'),
            'payway' => array('NAME' => 'payway')
        ),
        "TYPE" => "SELECT"
    )
);
